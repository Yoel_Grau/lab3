package SistemaEducativo;

public class Main {
    public static void main(String[] args) {
        ProfesorTitular p_titular = new ProfesorTitular(Materia.CIENCIA);
        ProfesorSuplente p_suplente = new ProfesorSuplente();

        System.out.println(p_titular.getID());
        System.out.println(p_titular.enseñar() + "\n");

        System.out.println(p_suplente.getID());
        p_suplente.asignar(Materia.MATEMATICAS);
        System.out.println(p_suplente.enseñar());

        p_suplente.asignar(Materia.CIENCIA);
        System.out.println(p_suplente.enseñar());

        p_suplente.asignar(Materia.GIMNASIA);
        System.out.println(p_suplente.enseñar());

        p_suplente.asignar(Materia.MUSICA);
        System.out.println(p_suplente.enseñar());
    }
}







