package SistemaEducativo;

public class ProfesorTitular extends Profesor implements Empleado {

    public ProfesorTitular(Materia materia) {
        super(materia);
    }

    @Override
    public String enseñar() {
        return "Dictando " + materia.name().toLowerCase();
    }

    @Override
    public String getID() {
        return "Profesor Titular";
    }
}








