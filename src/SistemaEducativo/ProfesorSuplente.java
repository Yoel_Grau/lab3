package SistemaEducativo;

public class ProfesorSuplente extends Profesor implements Empleado {

    public ProfesorSuplente() {
        super(null);
    }

    public void asignar(Materia materia) {
        this.materia = materia;
    }

    @Override
    public String enseñar() {
        if (this.materia == null) {
            return "Error: No hay materia asignada para el profesor suplente.";
        }
        return "Dictando " + materia.name().toLowerCase();
    }

    @Override
    public String getID() {
        return "Profesor Suplente";
    }
}






