package SistemaEducativo;

public abstract class Profesor {
    protected Materia materia;

    public Profesor(Materia materia) {
        this.materia = materia;
    }

    public abstract String enseñar();
}





