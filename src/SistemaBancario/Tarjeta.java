package SistemaBancario;

public interface Tarjeta {
    boolean retirar(double monto);
    void asociarCuenta(Cuenta cuenta);
}


