package SistemaBancario;

public abstract class Cuenta {
    protected double saldo;

    public Cuenta(double saldoInicial) {
        this.saldo = saldoInicial;
    }

    public double getSaldo() {
        return saldo;
    }

    public void depositar(double monto) {
        if (monto > 0) {
            saldo += monto;
        } else {
            System.out.println("Error: El valor a depositar tiene que ser positivo.");
        }
    }

    public abstract boolean retirar(double monto);
}
