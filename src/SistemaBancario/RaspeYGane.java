package SistemaBancario;

import java.util.Random;

public class RaspeYGane {

    private Random random = new Random();

    public TarjetaPrepago rasparYObtenerTarjeta() {
        if (random.nextInt(2) == 0) {
            System.out.println("Felicidades, ha ganado una tarjeta pre-pago!");
            return new TarjetaPrepago(600);
        } else {
            System.out.println("Lo sentimos, no ha ganado esta vez.");
            return null;
        }
    }
}
