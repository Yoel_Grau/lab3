package SistemaBancario;

public class TarjetaCredito implements Tarjeta {
    private Cuenta cuentaAsociada;
    private double creditoDisponible;

    public TarjetaCredito(double creditoDisponible) {
        this.creditoDisponible = creditoDisponible;
    }

    @Override
    public boolean retirar(double monto) {
        if (monto <= creditoDisponible && monto > 0) {
            creditoDisponible -= monto;
            return true;
        } else {
            System.out.println("Error: Credito insuficiente.");
            return false;
        }
    }


    @Override
    public void asociarCuenta(Cuenta cuenta) {
        this.cuentaAsociada = cuenta;
    }

    public double getCreditoDisponible() {
        return creditoDisponible;
    }
}


