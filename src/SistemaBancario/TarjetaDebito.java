package SistemaBancario;

public class TarjetaDebito implements Tarjeta {
    private Cuenta cuentaAsociada;

    @Override
    public boolean retirar(double monto) {
        if (cuentaAsociada != null && cuentaAsociada.getSaldo() >= monto && monto > 0) {
            return cuentaAsociada.retirar(monto);
        } else {
            System.out.println("Error: Fondos insuficientes en la tarjeta de debito.");
            return false;
        }
    }

    @Override
    public void asociarCuenta(Cuenta cuenta) {
        this.cuentaAsociada = cuenta;
    }
}

