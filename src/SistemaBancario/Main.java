package SistemaBancario;

public class Main {
    public static void main(String[] args) {


        CuentaCorriente cuentaCorriente = new CuentaCorriente(500);
        CuentaTemporal cuentaTemporal = new CuentaTemporal(100);

        TarjetaCredito tarjetaCredito = new TarjetaCredito(500);
        TarjetaDebito tarjetaDebito = new TarjetaDebito();
        TarjetaPrepago tarjetaPrepago = new TarjetaPrepago(100);

        tarjetaCredito.asociarCuenta(cuentaCorriente);
        tarjetaDebito.asociarCuenta(cuentaCorriente);
        tarjetaPrepago.asociarCuenta(cuentaTemporal);

        if (tarjetaCredito.retirar(50)) {
            System.out.println("Retiro exitoso!, Saldo en la tarjeta de crédito disponible: " + tarjetaCredito.getCreditoDisponible());
        }

        if (tarjetaDebito.retirar(200)) {
            System.out.println("Retiro exitoso!, Saldo en la tarjeta de cuenta corriente: " + cuentaCorriente.getSaldo());
        }

        if (tarjetaPrepago.retirar(90)) {
            System.out.println("Retiro exitoso!, Saldo en la tarjeta de cuenta temporal: " + cuentaTemporal.getSaldo());
        }

        RaspeYGane promocion = new RaspeYGane();
        TarjetaPrepago tarjetaGanada = promocion.rasparYObtenerTarjeta();

        if (tarjetaGanada != null) {
            System.out.println("El usuario ha ganado una tarjeta pre-pago con saldo de: " + tarjetaGanada.getSaldo());
            if (tarjetaGanada.retirar(50)) {
                System.out.println("Retiro exitoso!, Saldo de tarjeta pre-pago: " + tarjetaGanada.getSaldo());
            }
        }

    }
}
