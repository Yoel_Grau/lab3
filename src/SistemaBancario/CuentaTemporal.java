package SistemaBancario;

public class CuentaTemporal extends Cuenta {
    public CuentaTemporal(double saldoInicial) {
        super(saldoInicial);
    }

    @Override
    public boolean retirar(double monto) {
        if (monto <= saldo && monto > 0) {
            saldo -= monto;
            return true;
        } else {
            System.out.println("Error: Fondos insuficientes en la cuenta temporal.");
            return false;
        }
    }
}


