package SistemaBancario;

public class CuentaCorriente extends Cuenta {
    public CuentaCorriente(double saldoInicial) {
        super(saldoInicial);
    }

    @Override
    public boolean retirar(double monto) {
        if (monto <= saldo && monto > 0) {
            saldo -= monto;
            return true;
        } else {
            System.out.println("Error: Fondos insuficientes en la cuenta corriente.");
            return false;
        }
    }
}

