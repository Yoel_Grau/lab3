package SistemaBancario;

public class TarjetaPrepago implements Tarjeta {
    private CuentaTemporal cuentaAsociada;
    private EstadoTarjeta estado;

    public TarjetaPrepago(double saldoInicial) {
        this.estado = EstadoTarjeta.ACTIVA;
        this.cuentaAsociada = new CuentaTemporal(saldoInicial);
    }

    @Override
    public boolean retirar(double monto) {
        if (estado == EstadoTarjeta.DESHABILITADA) {
            System.out.println("Error: La tarjeta esta deshabilitada, no se pueden retirar fondos.");
            return false;
        }

        boolean resultado = cuentaAsociada.retirar(monto);
        if (resultado && cuentaAsociada.getSaldo() == 0) {
            estado = EstadoTarjeta.DESHABILITADA;
        }

        return resultado;
    }

    @Override
    public void asociarCuenta(Cuenta cuenta) {
        if (!(cuenta instanceof CuentaTemporal)) {
            System.out.println("Error: La tarjeta prepago solo se puede asociar con una cuenta temporal.");
        } else {
            this.cuentaAsociada = (CuentaTemporal) cuenta;
        }
    }

    public EstadoTarjeta getEstado() {
        return estado;
    }

    public double getSaldo() {
        return cuentaAsociada.getSaldo();
    }
}
