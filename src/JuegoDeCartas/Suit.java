package JuegoDeCartas;

public enum Suit {
    TREBOL('T'),
    DIAMANTES('D'),
    CORAZONES('C'),
    ESPADAS('E');
    private final char letter;

    Suit(char letter) {
        this.letter = letter;
    }

    public char getSymbol() {
        return this.letter;
    }
}





