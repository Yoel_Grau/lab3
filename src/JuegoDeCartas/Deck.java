package JuegoDeCartas;

import java.util.ArrayList;
import java.util.Random;

class Deck {
    private final ArrayList<Card> cards;
    private final Random random = new Random();

    public Deck() {
        this.cards = new ArrayList<>();
        for (Suit suit : Suit.values()) {
            for (char value : "23456789TJQKA".toCharArray()) {
                cards.add(new Card(suit, value));
            }
        }
        shuffleDeck();
    }

    private void shuffleDeck() {
        for (int i = cards.size() - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            Card a = cards.get(index);
            cards.set(index, cards.get(i));
            cards.set(i, a);
        }
    }

    public Card drawCard() {
        return cards.remove(cards.size() - 1);
    }

    public String compareCards(Card computerCard, Card playerCard) {
        int compare = computerCard.compareTo(playerCard);
        if (compare < 0) {
            return "Ganaste";
        } else if (compare > 0) {
            return "Perdiste";
        } else {
            return "Empate";
        }
    }
}



