package JuegoDeCartas;


public class Main {
    public static void main(String[] args) {
        Deck deck = new Deck();
        Card computerCard = deck.drawCard();
        Card playerCard = deck.drawCard();

        System.out.println("Computer: " + computerCard);
        System.out.println("Player: " + playerCard);

        System.out.println(deck.compareCards(computerCard, playerCard));
    }
}




