package JuegoDeCartas;

class Card implements Comparable<Card> {
    private final Suit suit;
    private final char value;

    public Card(Suit suit, char value) {
        this.suit = suit;
        this.value = value;
    }

    @Override
    public int compareTo(Card card) {
        String order = "23456789TJQKA";
        return order.indexOf(this.value) - order.indexOf(card.value);
    }

    @Override
    public String toString() {
        return "" + suit.getSymbol() + value;
    }
}







