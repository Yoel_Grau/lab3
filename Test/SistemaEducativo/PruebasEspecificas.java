package SistemaEducativo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class PruebasEspecificas {

    @Test
    void Prueba1() {
        ProfesorTitular titular = new ProfesorTitular(Materia.CIENCIA);
        assertEquals("Dictando ciencia", titular.enseñar());
    }

    @Test
    void Prueba2() {
        ProfesorSuplente suplente = new ProfesorSuplente();
        suplente.asignar(Materia.CIENCIA);
        assertEquals("Dictando ciencia", suplente.enseñar());
    }

    @Test
    void Prueba3() {
        ProfesorSuplente suplente = new ProfesorSuplente();
        suplente.asignar(Materia.GIMNASIA);
        assertEquals("Dictando gimnasia", suplente.enseñar());
    }

    @Test
    void Prueba4() {
        ProfesorSuplente suplente = new ProfesorSuplente();
        suplente.asignar(Materia.MUSICA);
        assertEquals("Dictando musica", suplente.enseñar());
    }
}





