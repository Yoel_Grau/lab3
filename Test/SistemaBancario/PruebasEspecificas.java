package SistemaBancario;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasEspecificas {

    @Test
    public void Prueba1() {
        CuentaCorriente c1 = new CuentaCorriente(500);
        TarjetaCredito t1 = new TarjetaCredito(500);
        t1.asociarCuenta(c1);

        t1.retirar(50);
        assertEquals(450, t1.getCreditoDisponible());
    }

    @Test
    public void Prueba2() {
        Cuenta c2 = new CuentaCorriente(100);
        Tarjeta t2 = new TarjetaCredito(500);
        t2.asociarCuenta(c2);

        t2.retirar(50);
        assertEquals(450, ((TarjetaCredito) t2).getCreditoDisponible());
    }

    @Test
    public void Prueba3() {
        TarjetaPrepago t3 = new TarjetaPrepago(100);
        t3.asociarCuenta(new CuentaTemporal(100));

        t3.retirar(100);
        assertEquals(0, t3.getSaldo());
        assertEquals(EstadoTarjeta.DESHABILITADA, t3.getEstado());
    }
}
