package JuegoDeCartas;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasEspecificas {

    @Test
    public void Prueba1() {
        Card computer1 = new Card(Suit.TREBOL, 'A');
        Card player1 = new Card(Suit.DIAMANTES, '2');
        Deck deck = new Deck();
        String result = deck.compareCards(computer1, player1);
        assertEquals("Perdiste", result);
    }

    @Test
    public void Prueba2() {
        Card computer2 = new Card(Suit.ESPADAS, '9');
        Card player2 = new Card(Suit.DIAMANTES, 'A');
        Deck deck = new Deck();
        String result = deck.compareCards(computer2, player2);
        assertEquals("Ganaste", result);
    }

    @Test
    public void Prueba3() {
        Card computerCard = new Card(Suit.TREBOL, 'A');
        Card playerCard = new Card(Suit.ESPADAS, 'A');
        Deck deck = new Deck();
        String result = deck.compareCards(computerCard, playerCard);
        assertEquals("Empate", result);
    }
}



